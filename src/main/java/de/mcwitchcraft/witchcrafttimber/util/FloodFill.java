package de.mcwitchcraft.witchcrafttimber.util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

public class FloodFill {
  private Location blockLoc;
  private double blockX;
  private double blockY;
  private double blockZ;
  private World world;
  private Material blockMat;
  private int breakCounter;

  public FloodFill(Block block){
    this.blockLoc = block.getLocation();
    this.blockMat = block.getType();
  }

  public boolean fill(){
    if(this.blockLoc != null){
      this.world = blockLoc.getWorld();
      this.blockX = blockLoc.getBlockX();
      this.blockY = blockLoc.getBlockY();
      this.blockZ = blockLoc.getBlockZ();
      floodFillBlock(this.world, this.blockX, this.blockY, this.blockZ);
    }else{
      return false;
    }
    return true;
  }

  private void floodFillBlock(World world, double x, double y, double z){
    Location thisBlockLoc = new Location(world, x, y, z);
    if(thisBlockLoc.getBlock().getType() == blockMat && breakCounter < 400){
      thisBlockLoc.getBlock().setType(Material.AIR);
      thisBlockLoc.getWorld().dropItemNaturally(thisBlockLoc, new ItemStack(blockMat));
      //prevents Lag and possible StackOverFlowErrors
      breakCounter++;

      try{
        floodFillBlock(world, x + 1, y, z);
        floodFillBlock(world, x - 1, y, z);
        floodFillBlock(world, x, y + 1, z);
        floodFillBlock(world, x, y - 1, z);
        floodFillBlock(world, x, y, z + 1);
        floodFillBlock(world, x, y, z - 1);
      }catch(StackOverflowError e){
        return;
      }
    }
    this.breakCounter = 0;
  }

}
