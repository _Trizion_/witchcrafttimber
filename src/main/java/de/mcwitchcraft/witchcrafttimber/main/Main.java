package de.mcwitchcraft.witchcrafttimber.main;

import de.mcwitchcraft.witchcrafttimber.listener.ChopWoodListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

  public void onEnable() {
    PluginManager pm = Bukkit.getPluginManager();
    pm.registerEvents(new ChopWoodListener(), this);
  }
}
