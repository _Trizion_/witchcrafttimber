package de.mcwitchcraft.witchcrafttimber.listener;

import de.mcwitchcraft.witchcrafttimber.util.FloodFill;
import java.util.Arrays;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class ChopWoodListener implements Listener {
  public static final List<Material> woodList = Arrays.asList(Material.ACACIA_LOG, Material.JUNGLE_LOG,
      Material.BIRCH_LOG, Material.OAK_LOG, Material.SPRUCE_LOG, Material.DARK_OAK_LOG);

  @EventHandler
  public void onWoodChop(BlockBreakEvent event){
    if(event.getPlayer().getInventory().getItemInMainHand().hasItemMeta()){
      if(woodList.contains(event.getBlock().getType()) &&
          event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals("Timber")){
        new FloodFill(event.getBlock()).fill();
      }
    }
  }
}
